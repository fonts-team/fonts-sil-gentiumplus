fonts-sil-gentiumplus (6.200-1) unstable; urgency=medium

  * New upstream release.
  * Add lintian override for HTML documentation files
  * Update Standards to 4.6.2 (checked)

 -- Bobby de Vos <bobby_devos@sil.org>  Tue, 19 Sep 2023 14:23:38 -0600

fonts-sil-gentiumplus (6.101-1) unstable; urgency=medium

  * New upstream release.

 -- Bobby de Vos <bobby_devos@sil.org>  Fri, 18 Feb 2022 10:02:56 -0700

fonts-sil-gentiumplus (6.001-3) unstable; urgency=medium

  * Update Standards to 4.6.0 (checked)

 -- Bobby de Vos <bobby_devos@sil.org>  Tue, 14 Dec 2021 08:38:10 -0700

fonts-sil-gentiumplus (6.001-2) unstable; urgency=medium

  * Bump debhelper from old 12 to 13.

 -- Bobby de Vos <bobby_devos@sil.org>  Mon, 13 Dec 2021 18:32:12 -0700

fonts-sil-gentiumplus (6.001-1) unstable; urgency=medium

  * New upstream release.

 -- Bobby de Vos <bobby_devos@sil.org>  Wed, 07 Jul 2021 09:58:32 -0600

fonts-sil-gentiumplus (6.000-2) unstable; urgency=medium

  * Bump version to re-upload

 -- Bobby de Vos <bobby_devos@sil.org>  Tue, 29 Jun 2021 13:43:36 -0600

fonts-sil-gentiumplus (6.000-1) unstable; urgency=medium

  * New upstream release.
  * Update Standards to 4.5.1 (checked)

 -- Bobby de Vos <bobby_devos@sil.org>  Tue, 29 Jun 2021 07:52:23 -0600

fonts-sil-gentiumplus (5.000-2) unstable; urgency=medium

  * Move WOFF files away from fontconfig.
  * Update packaging.
  * Update Standards to 4.1.3 (checked)

 -- Bobby de Vos <bobby_devos@sil.org>  Mon, 12 Feb 2018 15:29:03 -0700

fonts-sil-gentiumplus (5.000-1) unstable; urgency=medium

  * New upstream version
  * now includes woff webfont
  * d/copyright: fix formatting, years, upstream contact
  * Standards 3.9.6 (no changes)

 -- Daniel Glassey <wdg@debian.org>  Thu, 17 Sep 2015 15:23:53 +0100

fonts-sil-gentiumplus (1.510-web-2) unstable; urgency=low

  * Upload to Debian. Closes: #747574
  * Bump Standards to 3.9.5 (checked)
  * Set debhelper compatibility level to 9
  * Use 1.0 copyright format URL in debian/copyright
  * Use FONTLOG.txt as upstream changelog

 -- Christian Perrier <bubulle@debian.org>  Sat, 17 May 2014 15:06:10 +0200

fonts-sil-gentiumplus (1.510-web-1ubuntu2+precise) precise; urgency=low

  * Adjust dependencies

 -- Nicolas Spalinger <nicolas.spalinger@sil.org>  Tue, 26 Nov 2013 13:15:06 +0000

fonts-sil-gentiumplus (1.510-web-1ubuntu1+precise) precise; urgency=low

  * new upstream version (maintenance release with no developer version)
  * Update Standards to 3.9.3 (checked)
  * drop the old transitional package
  * Add "Multi-Arch: foreign" field

 -- Nicolas Spalinger <nicolas.spalinger@sil.org>  Mon, 25 Nov 2013 16:32:56 +0000

fonts-sil-gentiumplus (1.508-developer-1ubuntu8) oneiric; urgency=low

  * Rebuild for oneiric

 -- Nicolas Spalinger <nicolas.spalinger@sil.org>  Thu, 03 Nov 2011 20:49:48 +0100

fonts-sil-gentiumplus (1.508-developer-1ubuntu7) natty; urgency=low

  * Rebuild for natty

 -- Nicolas Spalinger <nicolas.spalinger@sil.org>  Thu, 03 Nov 2011 20:48:14 +0100

fonts-sil-gentiumplus (1.508-developer-1ubuntu6) maverick; urgency=low

  * Rebuild for maverick

 -- Nicolas Spalinger <nicolas.spalinger@sil.org>  Thu, 03 Nov 2011 20:46:26 +0100

fonts-sil-gentiumplus (1.508-developer-1ubuntu5) lucid; urgency=low

  * Adjust dependencies for name transition
  * Adjust Suggests
  * Adjust DEP5 syntax

 -- Nicolas Spalinger <nicolas.spalinger@sil.org>  Wed, 02 Nov 2011 12:18:45 +0100

fonts-sil-gentiumplus (1.508-developer-1ubuntu4) oneiric; urgency=low

  * rebuild for oneiric

 -- Nicolas Spalinger <nicolas.spalinger@sil.org>  Sat, 22 Oct 2011 19:31:04 +0200

fonts-sil-gentiumplus (1.508-developer-1ubuntu3) natty; urgency=low

  * rebuild for natty

 -- Nicolas Spalinger <nicolas.spalinger@sil.org>  Sat, 22 Oct 2011 19:29:28 +0200

fonts-sil-gentiumplus (1.508-developer-1ubuntu2) maverick; urgency=low

  * rebuild for maverick

 -- Nicolas Spalinger <nicolas.spalinger@sil.org>  Sat, 22 Oct 2011 19:28:42 +0200

fonts-sil-gentiumplus (1.508-developer-1ubuntu1) lucid; urgency=low

  * rebuild for lucid

 -- Nicolas Spalinger <nicolas.spalinger@sil.org>  Sat, 22 Oct 2011 19:27:11 +0200

fonts-sil-gentiumplus (1.508-developer-1) unstable; urgency=low

  * New upstream release
  * Small adjustment to naming according to pkg-fonts conventions

 -- Nicolas Spalinger <nicolas.spalinger@sil.org>  Sat, 22 Oct 2011 19:23:18 +0200

fonts-sil-gentium-plus (1.506-developer-1) unstable; urgency=low

  * New upstream release

 -- Nicolas Spalinger <nicolas.spalinger@sil.org>  Sat, 10 Sep 2011 22:45:50 +0200

fonts-sil-gentium-plus (1.504-developer-1ubuntu3) natty; urgency=low

  * rebuild for Natty

 -- Nicolas Spalinger <nicolas.spalinger@sil.org>  Mon, 11 Jul 2011 16:30:00 +0200

fonts-sil-gentium-plus (1.504-developer-1ubuntu2) maverick; urgency=low

  * rebuild for Maverick

 -- Nicolas Spalinger <nicolas.spalinger@sil.org>  Mon, 11 Jul 2011 15:44:36 +0200

fonts-sil-gentium-plus (1.504-developer-1ubuntu1) lucid; urgency=low

  * rebuild for Lucid

 -- Nicolas Spalinger <nicolas.spalinger@sil.org>  Mon, 11 Jul 2011 15:27:27 +0200

fonts-sil-gentium-plus (1.504-developer-1) unstable; urgency=low

  * New upstream release (Closes: #602905)

 -- Nicolas Spalinger <nicolas.spalinger@sil.org>  Tue, 30 Dec 2010 15:56:15 +0000
